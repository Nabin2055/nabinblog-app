from django.urls import path
from . import views

urlpatterns = [
    path('collection/', views.index, name='pfolio_list' ),
    path('collection/<int:pk>/', views.detail, name='pfolio_detail'),
]