from django.db import models

class Collection(models.Model):
    title = models.CharField(max_length=30)
    description = models.TextField()
    time = models.DateTimeField(auto_now_add=True)
    image = models.FileField(upload_to='images/')

    def __str__(self):
        return self.title