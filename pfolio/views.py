from django.shortcuts import render, get_object_or_404
from .models import Collection

def index(request):
    collection_list = Collection.objects.all()
    context = {'collection_list': collection_list}
    return render(request, 'index.html', context)

def detail(request, pk):
    collection = get_object_or_404(Collection, pk=pk)
    context = {'collection': collection}
    return render(request, 'detail.html', context)

